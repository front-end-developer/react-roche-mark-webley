/**
 * Created by Mark Webley on 04/10/2019.
 */
import React, {Component} from "react";
import * as Plottable from 'plottable';
import moment from "moment/moment";

// TODO: temporarily dissabled
// import style from './patient-data-component.module.scss';

export class PatientDataComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedPaitent: {
                fullname: '',
                data: {
                    dateOfBirth: '',
                    ranges: '',
                    timeBlocks: '',
                    glucoseMesures: ''
                }
            }
        };

    }

    buildGraph() {
        /******
         * TODO: make dynamic
         */

        const lowestRange = [
            {
                low: 85,
                date: moment("2015-06-12T00:49:50+00:00").format("hh:mm")
            },
            {
                low: 85,
                date: moment("2015-06-12T07:09:03+00:00").format("hh:mm")
            },
            {
                low: 85,
                date: moment("2015-06-12T11:23:15+00:00").format("hh:mm")
            },
            {
                low: 85,
                date: moment("2015-06-12T16:00:58+00:00").format("hh:mm")
            },
            {
                low: 85,
                date: moment("2015-06-12T19:35:01+00:00").format("hh:mm")
            },
            {
                low: 85,
                date: moment("2015-06-12T21:15:50+00:00").format("hh:mm")
            }
        ];

        const highestRange = [
            {
                high: 247,
                date: moment("2015-06-12T00:49:50+00:00").format("hh:mm")
            },
            {
                high: 247,
                date: moment("2015-06-12T07:09:03+00:00").format("hh:mm")
            },
            {
                high: 247,
                date: moment("2015-06-12T11:23:15+00:00").format("hh:mm")
            },
            {
                high: 247,
                date: moment("2015-06-12T16:00:58+00:00").format("hh:mm")
            },
            {
                high: 247,
                date: moment("2015-06-12T19:35:01+00:00").format("hh:mm")
            },
            {
                high: 247,
                date: moment("2015-06-12T21:15:50+00:00").format("hh:mm")
            }
        ];

        const glucoseMesures = [
            {
                glucose: 85,
                date: moment("2015-06-12T00:49:50+00:00").format("hh:mm")
            },
            {
                glucose: 77,
                date: moment("2015-06-12T07:09:03+00:00").format("hh:mm")
            },
            {
                glucose: 58,
                date: moment("2015-06-12T11:23:15+00:00").format("hh:mm")
            },
            {
                glucose: 110,
                date: moment("2015-06-12T16:00:58+00:00").format("hh:mm")
            },
            {
                glucose: 97,
                date: moment("2015-06-12T19:35:01+00:00").format("hh:mm")
            },
            {
                glucose: 142,
                date: moment("2015-06-12T21:15:50+00:00").format("hh:mm")
            }
        ];

        const glucoseDataSet = new Plottable.Dataset(glucoseMesures, { name: "Glucemia" });
        const glucoseLinePlot = new Plottable.Plots.Line();
        glucoseLinePlot.addDataset( glucoseDataSet );
        let yAccessor = (datum, index, dataset) => datum.glucose;
        let xAccessor = (datum) => datum.date;
        var yScaleGlucose = new Plottable.Scales.Linear();
        glucoseLinePlot.y(yAccessor, yScaleGlucose);
        const xScaleGlucose = new Plottable.Scales.Category();
        glucoseLinePlot.x(xAccessor, xScaleGlucose);
        const lowRangeDataSet = new Plottable.Dataset(lowestRange, { name: "Glucemia (mg/dL)" });
        const lowRangeLinePlot = new Plottable.Plots.Line();
        lowRangeLinePlot.addDataset( lowRangeDataSet );
        yAccessor = datum => datum.low;
        xAccessor = datum => datum.date;
        const yScale = new Plottable.Scales.Linear();
        lowRangeLinePlot.y(yAccessor, yScale);
        lowRangeLinePlot.attr("stroke", "#db2e4c");
        const xScale = new Plottable.Scales.Category();
        lowRangeLinePlot.x(xAccessor, xScale);
        const yAxis = new Plottable.Axes.Numeric(yScale, "left");
        const xAxis = new Plottable.Axes.Category(xScale, "bottom");
        const xLabel = new Plottable.Components.AxisLabel("label at chart base");
        const yLabel = new Plottable.Components.AxisLabel(lowRangeDataSet.metadata().name, 270);
        const highestRangeDataSet = new Plottable.Dataset(highestRange, { name: "Dosis basal (U/h))" });
        const yHighRangeScale = new Plottable.Scales.Linear();
        const highRangePlot = new Plottable.Plots.Line();
        highRangePlot.addDataset( highestRangeDataSet );
        const yAccessor2 = d => d.high;
        const xAccessor2 = d => d.date;
        highRangePlot.x(xAccessor2, xScale);
        highRangePlot.y(yAccessor2, yScale);
        highRangePlot.attr("stroke", "#333333");
        const yHighRangeAxis = new Plottable.Axes.Numeric(yHighRangeScale, "left");
        const yHighRangeLabel = new Plottable.Components.AxisLabel(highestRangeDataSet.metadata().name, 270);
        const group = new Plottable.Components.Group([
            lowRangeLinePlot,
            highRangePlot,
            glucoseLinePlot
        ]);
        const table = new Plottable.Components.Table([
            [yLabel, yAxis, group, yHighRangeAxis, yHighRangeLabel],
            [null, null, xAxis, null, null],
            [null, null, xLabel, null, null ]
        ]);

        table.renderTo(".patient-data");

    }

    componentDidMount() {
        setTimeout(() => this.buildGraph(), 1000);
    }

    // {`{card shadow-sm ${style.cardChart}}`}
    // `{patient-data ${style.chart}}`}
    // TODO: remove inline style
    render() {
        return (
            <div className='container chart'>
                <div className="card shadow-sm" style={{height: '500px', paddingBottom: '35px', marginBottom: '25px'}}>
                    <div className="card-body">
                        <div className="patient-data" width="200" height="260px">Chart Test</div>
                    </div>
                </div>
            </div>
        )
    }

};
