/**
 * Created by Mark Webley on 03/10/2019.
 */
export const APISERVICES = {
    API: {
        GET_PRACTITIONERS: 'http://www.wisdomor.co.uk/exercises_1/patients/index.php',
        GET_PATIENTS: 'http://www.wisdomor.co.uk/exercises_1/patients/index.php'
    },

    CHART: {
        ASSET_CHART_DATA: 'https://raw.githubusercontent.com/plotly/datasets/master/2014_apple_stock.csv'
    }
};
