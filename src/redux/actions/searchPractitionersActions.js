/**
 * Created by Mark Webley on 04/10/2019.
 */
import * as types from './allActionTypes';

export function searchPractitionersActions(practitioner) {
    return {
        type: types.SEARCH_PRACTITIONAR,
        practitioner,
        error: ''
    };
}

export function storePractitionerAction(practitioner) {
    return {
        type: types.STORE_PRACTITIONAR,
        practitioner,
        error: ''
    };
}

