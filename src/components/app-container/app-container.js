/**
 * Created by Mark Webley on 02/10/2019.
 */
import React, {Component} from 'react';
import AppRoutes from "../common/routes/app-routes";

export default class AppContainer extends Component {
    render() {
        return (
            <>
                <AppRoutes />
            </>
        );
    }
}
