/**
 * Created by Mark Webley on 09/10/2019.
 */
import React from 'react';
import ReactDOM from 'react-dom';
import ListPatientsComponent from '../list-patients-component';
import configureStore from '../../../redux/configureStore';
import * as types from '../../../redux/actions/allActionTypes';
import * as actions from '../../../redux/actions/loadPatientsActions';

describe('Scenario: user lands on the patients page', () => {
    it('Given loadPatientsAction should be dispatched, should create an action to load all paitents', () => {
        const patients = [
            {
                "patientId": "5cc17bd0b822ffcbf1afb918",
                "practitionerId": "2588ac7f57fd9b49",
                "name": "Elena",
                "surname": "Holden",
                "fullname": "Elena Holden",
                "dateOfBirth": "1980-03-19T13:00:51+00:00",
                "diabetesType": "DIABETES_TYPE2",
                "ranges": {
                    "low": 54,
                    "ideal": {
                        "from": 66,
                        "to": 94
                    },
                    "high": 248
                },
                "timeBlocks": {
                    "night": {
                        "from": "00:00",
                        "to": "06:00"
                    },
                    "breakfast": {
                        "from": "06:00",
                        "to": "12:00"
                    },
                    "lunch": {
                        "from": "12:00",
                        "to": "18:00"
                    },
                    "dinner": {
                        "from": "18:00",
                        "to": "00:00"
                    }
                },
                "glucoseMesures": [
                    {
                        "glucose": 95,
                        "date": "2019-11-25T00:51:09+00:00"
                    },
                    {
                        "glucose": 119,
                        "date": "2019-11-25T06:39:24+00:00"
                    },
                    {
                        "glucose": 7,
                        "date": "2019-11-25T09:34:16+00:00"
                    },
                    {
                        "glucose": 132,
                        "date": "2019-11-25T13:05:37+00:00"
                    },
                    {
                        "glucose": 60,
                        "date": "2019-11-25T18:17:03+00:00"
                    },
                    {
                        "glucose": 140,
                        "date": "2019-11-25T21:05:51+00:00"
                    }
                ]
            },
            {
                "patientId": "5cc17bd0d18dd769c669912a",
                "practitionerId": "2588ac7f57fd9b49",
                "name": "Cecelia",
                "surname": "Hale",
                "fullname": "Cecelia Hale",
                "dateOfBirth": "1993-06-11T03:40:08+00:00",
                "diabetesType": "DIABETES_TYPE1",
                "ranges": {
                    "low": 21,
                    "ideal": {
                        "from": 88,
                        "to": 109
                    },
                    "high": 240
                },
                "timeBlocks": {
                    "night": {
                        "from": "00:00",
                        "to": "06:00"
                    },
                    "breakfast": {
                        "from": "06:00",
                        "to": "12:00"
                    },
                    "lunch": {
                        "from": "12:00",
                        "to": "18:00"
                    },
                    "dinner": {
                        "from": "18:00",
                        "to": "00:00"
                    }
                },
                "glucoseMesures": [
                    {
                        "glucose": 83,
                        "date": "2018-06-08T00:36:39+00:00"
                    },
                    {
                        "glucose": 6,
                        "date": "2018-06-08T07:52:01+00:00"
                    },
                    {
                        "glucose": 59,
                        "date": "2018-06-08T10:39:25+00:00"
                    },
                    {
                        "glucose": 119,
                        "date": "2018-06-08T12:17:14+00:00"
                    },
                    {
                        "glucose": 134,
                        "date": "2018-06-08T16:20:01+00:00"
                    },
                    {
                        "glucose": 100,
                        "date": "2018-06-08T19:44:39+00:00"
                    },
                    {
                        "glucose": 153,
                        "date": "2018-06-08T21:28:53+00:00"
                    }
                ]
            },
            {
                "patientId": "5cc17bd0db576b9510c8f9fa",
                "practitionerId": "2588ac7f57fd9b49",
                "name": "Kinney",
                "surname": "Anderson",
                "fullname": "Kinney Anderson",
                "dateOfBirth": "2000-03-01T05:50:49+00:00",
                "diabetesType": "DIABETES_TYPE2",
                "ranges": {
                    "low": 11,
                    "ideal": {
                        "from": 75,
                        "to": 109
                    },
                    "high": 244
                },
                "timeBlocks": {
                    "night": {
                        "from": "00:00",
                        "to": "06:00"
                    },
                    "breakfast": {
                        "from": "06:00",
                        "to": "12:00"
                    },
                    "lunch": {
                        "from": "12:00",
                        "to": "18:00"
                    },
                    "dinner": {
                        "from": "18:00",
                        "to": "00:00"
                    }
                },
                "glucoseMesures": [
                    {
                        "glucose": 133,
                        "date": "2015-09-13T00:42:01+00:00"
                    },
                    {
                        "glucose": 107,
                        "date": "2015-09-13T08:58:36+00:00"
                    },
                    {
                        "glucose": 44,
                        "date": "2015-09-13T09:27:41+00:00"
                    },
                    {
                        "glucose": 120,
                        "date": "2015-09-13T12:56:22+00:00"
                    },
                    {
                        "glucose": 72,
                        "date": "2015-09-13T14:04:45+00:00"
                    },
                    {
                        "glucose": 63,
                        "date": "2015-09-13T18:01:07+00:00"
                    }
                ]
            },
        ];
        const expectedAction = {
            type: types.LOAD_SUCCESS,
            patients,
            error: ''
        }
        expect(actions.loadPatientSuccessAction(patients)).toEqual(expectedAction)
    });

    it('Initialise the Redux store, pass in a practitionerId & load ListPatientsComponent without error', () => {
        console.log('results');
        const store = configureStore();
        const div = document.createElement('div');
        const practitionerId = '2588ac7f57fd9b49';
        ReactDOM.render(<ListPatientsComponent store={store} practitionerId={practitionerId} />, div);
        ReactDOM.unmountComponentAtNode(div);
    });
});

