/**
 * Created by Mark Webley on 03/10/2019.
 */
import * as types from './allActionTypes';
export function loadPatientsAction() {
    return {
        type: types.LOAD_PATIENTS,
        error: ''
    };
}

export function loadPatientSuccessAction(patients) {
    return {
        type: types.LOAD_SUCCESS,
        patients,
        error: ''
    };
}

export function loadPatientsErrorAction(message) {
    return {
        type: types.LOAD_ERROR,
        error: message
    };
}
