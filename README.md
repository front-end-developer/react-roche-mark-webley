# AUTHOR: MARK WEBLEY

Typescript version of this is here:
https://bitbucket.org/front-end-developer/react-roche-mark-webley-typescript/src/master/

#### TEST URLS:
http://localhost:3000/

http://localhost:3000/practitioner/3

## Project Explained

I placed a copy of your data.json file here on my personal
website wisdomer.co.uk, so I can play with it using redux-obervables and epics,
instead of doing it locally:
http://www.wisdomor.co.uk/exercises_1/patients/index.php

You can test the with the practitioner ID: 2588ac7f57fd9b49

### Chart
I used hardcoded data to test the chart during development to get expected results, I will change it to dynamic later as a separate component.
I started building the chart in Plottable.js but realised YET GAIN, there is not enougth tutorial resources (I guess Palantir the creators want to monopolise on that fact as being the best service providers) to do what I want to do quickly, so I regretted I did not use plot.ly.js which would have been quicker & straight forward, then I ran out of time, then I realised perhapd it would have been quicker if I just done it all in D3 manually.

### PRACTITIONER (HOME) SCREEN
I have some code set up for the practitioner screen for typeahead but that works with the Epics,
but I never got time to finish the typeahead work.

### My Focus
I focused on the boostrapping the Redux, Reduc-Observer and the UI Styling and look and feel.

Then on the structure of the actions for redux and the epics etc.

Then the flow of data, deciding should I use redux payloads to send data to the chart of just past state to a chilld components props, so I took the quicker option.
I would prefer to use Redux, Acion and reducers then the data dispatched can be used with other widgets :).

So I focused on the architecture, the UI and the main functionalites.

## Available Scripts

In the project directory, you can run:

### global install the following:
npm -g tslint typescript tslint-react

### `npm install`

### `npm start`

### Get up and running with:

`npm run build`

`npm run start`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### WEB SERVICES
I could do  a call to the service to init the app with all the data like a call to an api/AllData, but instead I simulated common scenario where we call only data when it is needed, and we expect the backend to return results only for not is needed.

Also as a note, I have worked with some apps where they load all the data from the begining, but sometimes they are the most difficult platforms to work with, because there is like 1o years worth of data, and data structures by then employees have left and many people do now know what the data is, or what goes where, or what is depreciated, do those problems extend development times.

This is why for me, platform architecture is important and I pay attention to that area naturally.

Having a strucuted webservice (perhaps use swagger to help), is more efficient in development time and maintenence.

### Project includes:
- modular App architecture
- React, redux, rxjs, Redux-observables, epic, store etc
- two test cases, pass in a Redux store, practitionerID and load the component
- added d3, and another charting library, just a sample.


### Redux sending payload to the store
Type & use paitent ID codes:

2588ac7f57fd9b49

2588ac7f57fd9b49

![Alt text](screenshots/search-for-patients-via-practitioner-id.jpg "Redux, Flux, Actions")

### Redux listen for changes to the store
![Alt text](screenshots/list-practitioners-paitents.jpg "Redux, Flux")





