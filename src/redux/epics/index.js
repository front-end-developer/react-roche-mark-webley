/**
 * Created by Mark Webley on 03/10/2019.
 */
import { combineEpics, ofType } from 'redux-observable';
import { of } from 'rxjs';
import {ajax} from 'rxjs/ajax';
import {map, switchMap, catchError} from 'rxjs/operators';
import {APISERVICES}  from '../../services/APIServices';
import * as types from '../actions/allActionTypes';

const fetchPaitentsEpic = action$ => (
    action$.pipe(
        ofType(types.LOAD_PATIENTS),
        switchMap(() => {
            return ajax.getJSON(APISERVICES.API.GET_PATIENTS)
                .pipe(
                    map((data) => {
                        return {
                            type: types.LOAD_SUCCESS,
                            patients: data
                        }
                    }),
                    catchError((error) => {
                        console.log('error: ', error);
                        return of({
                            type: types.LOAD_ERROR,
                            error: error
                        });
                    })
                )
        })
    )
)

/**
 * @description imaging this was coming from a different API service
 *              in the real world the backend server should filter the results according to the
 *              ID sent to it
 */
const fetchPractitionersEpic = action$ => (
    action$.pipe(
        ofType(types.SEARCH_PRACTITIONAR),
        switchMap((practice) => {
            return ajax.getJSON(APISERVICES.API.GET_PRACTITIONERS)
                .pipe(
                    map(data => {
                        const isPractitioner = data.find(function(patient) {
                            return patient.practitionerId === practice.practitioner;
                        });
                        return {
                            type: types.LOAD_PRACTITIONAR_SUCCESS,
                            practitioner: (typeof isPractitioner !== 'undefined' ? isPractitioner.practitionerId : null)
                        }
                    }),
                    catchError(error => {
                        console.log('error: ', error);
                        return of({
                            type: types.LOAD_PRACTITIONAR_ERROR,
                            error: error
                        });
                    })
                )
        })
    )
)

export const rootEpic = combineEpics(fetchPaitentsEpic, fetchPractitionersEpic);
