/**
 * Created by Mark Webley on 02/10/2019.
 */
export const LOAD_PATIENTS: string = 'LOAD_PATIENTS';
export const LOAD_SUCCESS: string = 'LOAD_SUCCESS';
export const LOAD_ERROR: string = 'LOAD_ERROR';
export const CHART_ONE_SELECTED_DATA: string = 'CHART_ONE_SELECTED_DATA';
export const STORE_PRACTITIONAR: string = 'STORE_PRACTITIONAR';
export const SEARCH_PRACTITIONAR: string = 'SEARCH_PRACTITIONAR';
export const LOAD_PRACTITIONAR_SUCCESS: string = 'LOAD_PRACTITIONAR_SUCCESS';
export const LOAD_PRACTITIONAR_ERROR: string = 'LOAD_PRACTITIONAR_ERROR';
