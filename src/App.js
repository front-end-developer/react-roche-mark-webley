import React from 'react';
import AppContainer from './components/app-container/app-container';
import './App.css';

function App() {
  return (
    <div className="App">
          <AppContainer />
    </div>
  );
}

export default App;
